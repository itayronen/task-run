# task-run
Task runner, written in typescript.
Task implements the [task-api](https://gitlab.com/itayronen/task-api)

# Install
`npm install task-run`

Amd and systemjs bundles included.

# Usage
You can check the [test file](https://gitlab.com/itayronen/task-run/blob/master/src/Tasks.test.ts) for all the examples.
```ts
let task = Tasks.run(() => { return 3; });
assert.equal(await task, 3);
```
```ts
let task = Tasks.run(() => { return "Hi"; });
await task.end;

assert.equal(task.status, Status.Succeded);
```
```ts
let task = Tasks.run<number>(() => { throw "Some error"; });

assert.throws(async () => await task);
```
```ts
let task = Tasks.run(() => { throw "Some error"; });
await task.end;

assert.equal(task.status, Status.Failed);
```
```ts
let source = new CancelTokenSource();
source.cancel();

let task = Tasks.runCancelable((token) => { token.throwIfCancelRequested(); }, source.token);
await task.end;

assert.equal(task.status, Status.Cancelled);
```

```ts
let task = Tasks.succeded(4);
assert.equal(await task, 4);
```
```ts
let task = Tasks.failed("The reason is you.");
assert.throws(async () => await task, "The reason is you.");
```
```ts
let task = Tasks.cancelled();
assert.throws(async () => await task);
assert.equal(task.status, Status.Cancelled);
```

```ts
let controller = Tasks.createController<string>();
controller.setSucceded("Hi");

assert.equal(await controller.task, "Hi");
```
```ts
let controller = Tasks.createController<number>();
controller.setFailed("some reason");

assert.throws(async () => await controller.task, "some reason");
```
```ts
let controller = Tasks.createController<number>();
controller.setCancelled();

assert.equal(controller.task.status, Status.Cancelled);
```