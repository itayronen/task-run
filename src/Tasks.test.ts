import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";
import { Tasks, Status, CancelTokenSource, CancelToken } from "./Tasks";

export default function (suite: TestSuite): void {
	suite.describe(Tasks, suite => {

		suite.describe("When task is running", suite => {
			suite.test(`The task status is ${Status[Status.Running]}.`, async test => {
				test.act();
				let task = Tasks.fromPromise(new Promise(resolve => setTimeout(resolve, 1)));

				test.assert();
				assert.equal(task.status, Status.Running);
			});
		});

		suite.describe("When task is successful", suite => {

			suite.describe("When sync func passed to task", suite => {

				suite.test("Awaitng a task returns the value.", async test => {
					test.act();
					let task = Tasks.run(() => { return 3; });

					test.assert();
					assert.equal(await task, 3);
				});

				suite.test(`When sync func passed to the task, status is immediately ${Status[Status.Succeeded]}.`, async test => {
					test.act();
					let task = Tasks.run(() => { return "Hi"; });

					test.assert();
					assert.equal(task.status, Status.Succeeded);
				});
			});

			suite.describe("When sync func passed to task", suite => {
				suite.test("Awaiting a task that runs async function, returns the value.", async test => {
					test.act();
					let task = Tasks.run(async () => { return Promise.resolve(3); });

					test.assert();
					assert.equal(await task, 3);
				});

				suite.test(`The task status is ${Status[Status.Succeeded]}.`, async test => {
					test.act();
					let task = Tasks.run(() => { return "Hi"; });
					await task.end;

					test.assert();
					assert.equal(task.status, Status.Succeeded);
				});
			});

			suite.test("When using fromPromise, awaiting the task returns the value.", async test => {
				test.act();
				let task = Tasks.fromPromise(Promise.resolve(3));

				test.assert();
				assert.equal(await task, 3);
			});
		});

		suite.describe("When error thrown", suite => {
			suite.test("Awaiting the task throws.", async test => {
				test.act();
				let task = Tasks.run<number>(() => { throw "Some error"; });

				test.assert();
				try { await task; } catch (error) { return; }

				assert.fail("Should have thrown.");
			});

			suite.test("When init with async function, and throwing after await, and awaiting the task, throws.", async test => {
				test.act();
				let task = Tasks.run(async () => {
					await new Promise(r => setTimeout(r, 1));
					throw "Some error";
				});

				test.assert();
				try {
					await task;
					assert.fail("Should have thrown.");
				}
				catch (error) { }
			});

			suite.test(`Task status is ${Status[Status.Failed]}.`, async test => {
				test.act();
				let task = Tasks.run(() => { throw "Some error"; });
				await task.end;

				test.assert();
				assert.equal(task.status, Status.Failed);
			});
		});

		suite.describe("When task started with runCancelable is cancelled", suite => {
			suite.test("Awaiting the task throws.", async test => {
				test.arrange();
				let source = new CancelTokenSource();
				source.cancel();

				test.act();
				let task = Tasks.runCancelable((token) => { token.throwIfCancelRequested(); }, source.token);

				test.assert();
				try { await task; } catch (error) { return; }
				assert.fail("Should have thrown.");
			});

			suite.test(`Task Status is ${Status[Status.Cancelled]}.`, async test => {
				test.arrange();
				let source = new CancelTokenSource();
				source.cancel();

				test.act();
				let task = Tasks.runCancelable((token) => { token.throwIfCancelRequested(); }, source.token);
				await task.end;

				test.assert();
				assert.equal(task.status, Status.Cancelled);
			});
		});

		suite.describe("When task started with run is cancelled", suite => {
			suite.test(`Task Status is ${Status[Status.Cancelled]}.`, async test => {
				test.arrange();
				let source = new CancelTokenSource();
				source.cancel();

				test.act();
				let task = Tasks.run(() => { source.token.throwIfCancelRequested(); });
				await task.end;

				test.assert();
				assert.equal(task.status, Status.Cancelled);
			});
		});

		suite.describe("Controller", suite => {
			suite.test("Integration", async test => {
				test.act();
				let controller = Tasks.createController<string>();
				controller.setSucceded("Hi");

				assert.equal(await controller.task, "Hi");
			});
		});

		suite.describe(Tasks.succeded.name, suite => {
			suite.test("Integration", async test => {
				test.act();
				let task = Tasks.succeded(4);

				test.assert();
				assert.equal(task.status, Status.Succeeded);
				assert.equal(await task, 4);
			});

			suite.test("Integration NOT async method (Yes it was an actual bug).", test => {
				test.act();
				let task = Tasks.succeded<void>(undefined);

				test.assert();
				assert.equal(task.status, Status.Succeeded);
			});
		});

		suite.describe(Tasks.failed.name, suite => {
			suite.test("Integration", async test => {
				test.act();
				let task = Tasks.failed("The reason is you.");

				test.assert();
				assert.equal(task.status, Status.Failed);

				try { await task; } catch (error) { return; }
				assert.fail("Should have thrown.");
			});
		});

		suite.describe(Tasks.cancelled.name, suite => {
			suite.test("Integration", async test => {
				test.act();
				let task = Tasks.cancelled();

				test.assert();
				assert.equal(task.status, Status.Cancelled);

				try { await task; } catch (error) { return; }
				assert.fail("Should have thrown.");
			});
		});
	});
}