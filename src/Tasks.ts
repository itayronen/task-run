import { Task, Status } from "task-api";
import { CancelToken } from "task-cancel";
import { MyTask } from "./MyTask";
import { TaskController } from "./TaskController";

export * from "task-cancel";
export * from "task-api";

export class Tasks {
	public static run<TResult>(func: () => TResult | PromiseLike<TResult>): Task<TResult> {
		return new MyTask(func);
	}

	public static runCancelable<TResult>(
		func: (cancelToken: CancelToken) => TResult | PromiseLike<TResult>,
		cancelToken: CancelToken
	): Task<TResult> {
		return new MyTask(() => func(cancelToken));
	}

	public static fromPromise<TResult>(promise: Promise<TResult>): Task<TResult> {
		return new MyTask<TResult>(() => promise);
	}

	public static createController<TResult>(): TaskController<TResult> {
		return new TaskController<TResult>();
	}

	public static succeded<TResult>(result: TResult): Task<TResult> {
		return MyTask.succeded(result);
	}

	public static failed<TResult>(reason?: any): Task<TResult> {
		return MyTask.failed(reason);
	}

	public static cancelled<TResult>(): Task<TResult> {
		return MyTask.cancelled();
	}
}