import { MyTask, MyTaskController } from "./MyTask";
import { Task, Status, CancelError } from "./Tasks";

export class TaskController<TResult> {
	private _task: Task<TResult>;
	private resolve!: (result: TResult) => void;
	private reject!: (reason?: any) => void;
	private controller = {} as MyTaskController;

	constructor() {
		let promise = new Promise<TResult>((a, b) => { this.resolve = a, this.reject = b });

		this._task = new MyTask(() => promise, this.controller);

		// To avoid unhandled rejected promises.
		// A controller is not really async therefore it is intuitive to not await it.
		this._task.catch(() => { });
	}

	public get task(): Task<TResult> {
		return this._task;
	}

	public setSucceded(result: TResult): void {
		this.controller.setStatus(Status.Succeeded);
		this.resolve(result);
	}

	public setFailed(reason?: any): void {
		reason = reason || "Task failed.";

		this.controller.setStatus(Status.Failed);
		this.controller.setError(reason);

		this.reject(reason);
	}

	public setCancelled(): void {
		this.controller.setStatus(Status.Cancelled);
		this.reject(new CancelError());
	}
}