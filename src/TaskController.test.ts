import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";

import { TaskController } from "./TaskController";
import { Status } from "task-api";

export default function (suite: TestSuite): void {
	suite.describe(TaskController.name, suite => {

		suite.test(`When created, the status is ${Status[Status.Running]}.`, async test => {
			test.arrange();
			let controller = new TaskController<void>();

			test.assert();
			assert.equal(controller.task.status, Status.Running);
		});

		suite.describe("When set to succeded", async suite => {
			suite.test(`The status is ${Status[Status.Succeeded]}.`, async test => {
				test.arrange();
				let controller = new TaskController<void>();

				await new Promise(r => setTimeout(r, 1));

				controller.setSucceded(undefined);

				test.assert();
				assert.equal(controller.task.status, Status.Succeeded);
			});

			suite.test(`The result passed is returned at await.`, async test => {
				test.arrange();
				let controller = new TaskController<string>();
				controller.setSucceded("Hi");

				test.assert();
				assert.equal(await controller.task, "Hi");
			});
		});

		suite.describe("When set to failed", suite => {
			suite.test(`The status is ${Status[Status.Failed]}.`, async test => {
				test.arrange();
				let controller = new TaskController<void>();
				controller.setFailed();

				test.assert();
				assert.equal(controller.task.status, Status.Failed);
			});

			suite.test(`The reason passed is thrown at await.`, async test => {
				test.arrange();
				let controller = new TaskController<number>();
				controller.setFailed("some reason");

				test.assert();

				try { await controller.task; } catch (error) { if (error == "some reason") return; }
				assert.fail("Should have thrown.");
			});

			suite.test(`When no reason provided, throws at await.`, async test => {
				test.arrange();
				let controller = new TaskController<number>();
				controller.setFailed();

				test.assert();
				
				try { await controller.task; } catch (error) { return; }
				assert.fail("Should have thrown.");
			});
		});

		suite.describe("When set to cancelled", suite => {
			suite.test(`The status is ${Status[Status.Cancelled]}.`, async test => {
				test.arrange();
				let controller = new TaskController<number>();
				controller.setCancelled();

				test.assert();
				assert.equal(controller.task.status, Status.Cancelled);
			});
		});
	});
}