import { Task, Status } from "task-api";
import { CancelToken, CancelError } from "task-cancel";

export interface MyTaskController {
	setStatus: (status: Status) => void;
	setError: (error: any) => void;
}

export class MyTask<TResult> implements Task<TResult>{
	private _end?: Promise<void>;
	private _status: Status = Status.Running;
	private _error: any;
	private promise: Promise<TResult>;

	private isPromiseCatched = false;

	public constructor(func: () => TResult | PromiseLike<TResult>, controller?: MyTaskController) {
		if (controller) {
			controller.setError = error => this._error = error;
			controller.setStatus = status => this._status = status;
		}

		try {
			let result = func();

			if (this.isPromiseLike(result)) {
				this.promise = this.createTaskPromise(result as PromiseLike<TResult>);
			}
			else {
				this.promise = Promise.resolve(result);
				this._status = Status.Succeeded;
			}
		}
		catch (error) {
			this._error = error;
			this.setStatusByError(error);
			this.promise = Promise.reject(error);
			// this.promise.catch(() => { }); // Need for unhandled promise?
		}

		this.then = (a, b) => this.promise.then(a, b);
		this.catch = a => { this.isPromiseCatched = true; return this.promise.catch(a); }
	}

	private isPromiseLike(obj: any): boolean {
		return obj != undefined &&
			typeof (obj as PromiseLike<TResult>).then === "function";
	}

	private createTaskPromise<T>(promise: PromiseLike<T>): Promise<T> {
		return Promise.resolve(promise).then(
			result => {
				this._status = Status.Succeeded;

				return result;
			},
			reason => {
				this._error = reason;

				this.setStatusByError(this._error);

				throw this._error;
			});
	}

	private setStatusByError(reason?: any): void {
		if (reason instanceof CancelError) {
			this._status = Status.Cancelled;
		}
		else {
			this._status = Status.Failed;
		}
	}

	public static succeded<TResult>(result: TResult): MyTask<TResult> {
		return new MyTask(() => result);
	}

	public static failed<TResult>(reason?: any): MyTask<TResult> {
		return new MyTask(() => { throw reason || "Task failed."; });
	}

	public static cancelled<TResult>(): MyTask<TResult> {
		return new MyTask(() => { throw new CancelError(); });
	}

	public get status(): Status {
		if (this.isEnded) this.ensureCatched();

		return this._status;
	}

	public get error(): any { return this._error; }

	public get end(): Promise<void> {
		if (!this._end) {
			if (this.isEnded) {
				this._end = Promise.resolve();
			}

			this._end = new Promise<void>((resolve, reject) => {
				this.promise.then(() => resolve(), () => resolve());
			});
		}

		return this._end;
	}

	public get isEnded(): boolean { return this._status != Status.Running; }

	public get isRunning(): boolean { return this._status == Status.Running; }

	public get isSucceded(): boolean {
		this.ensureCatched();

		return this._status == Status.Succeeded;
	}

	public get isFailed(): boolean {
		this.ensureCatched();

		return this._status == Status.Failed;
	}

	public get isCancelled(): boolean {
		this.ensureCatched();

		return this._status == Status.Cancelled;
	}

	public then: <TResult1 = TResult, TResult2 = never>(
		onfulfilled?: ((value: TResult) => TResult1 | PromiseLike<TResult1>) | null,
		onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | null
	) => Promise<TResult1 | TResult2>;

	public catch: <TResult2 = never>(
		onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | null
	) => Promise<TResult | TResult2>;

	get [Symbol.toStringTag](): "Promise" { return "Task" as "Promise"; }


	private ensureCatched(): void {
		if (this.isPromiseCatched) return;

		this.isPromiseCatched = true;
		this.promise.catch(() => { });
	}
}